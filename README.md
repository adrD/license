# End-User License Agreement

Last updated: November 18, 2018

Please read this End-User License Agreement ("Agreement") carefully before using Vertretungsapp
("Application").

By clicking the "I Agree" button, downloading or using the Application, you
are agreeing to be bound by the terms and conditions of this Agreement.

This Agreement is a legal agreement between you (either an individual or a
single entity) and the developers of Vertretungsapp and it governs your use of the Application
made available to you by the developers of Vertretungsapp.

If you do not agree to the terms of this Agreement, do not download or use the Application.

The Application is licensed, not sold, to you by Vertretungsapp for use
strictly in accordance with the terms of this Agreement.

License

Vertretungsapp grants you a revocable, non-exclusive, non-transferable,
license to download, install and use the Application solely for your
personal, non-commercial purposes strictly in accordance with the terms of
this Agreement.
You shall not decompile, reverse engineer, disassemble, include in other software,
or translate the Software, or use the Software for any commercial purposes.
You shall not modify, alter, change or otherwise make any modification to the
Software or create derivative works based upon the Software.
You shall not rent, lease, resell, sub-license, assign, distribute or otherwise transfer
the Software or this license. Any attempt to do so shall be void and of no effect.


Third-Party Services

The Application displays, includes or makes available third-party content.
In particular it shows the third-party website 'www.mpgg.de'. You acknowledge and agree 
that Vertretungsapp shall not be responsible for any Third-Party Services, 
including their accuracy, completeness, timeliness, validity, copyright compliance, 
legality, decency, quality or any other aspect thereof. 
Vertretungsapp does not assume and shall not have any liability or
responsibility to you or any other person or entity for any Third-Party
Services.

Third-Party Services and links thereto are provided solely as a convenience to
you and you access and use them entirely at your own risk and subject to such
third parties' terms and conditions.

Responsibility for the accuracy of the content

The developers of Vertretungsapp are not responsible for the correctness
of the displayed content. Especially, they are not liable for possible
misunderstandings caused by incorrect entries in the substitution plan.

Term and Termination

This Agreement shall remain in effect until terminated by you or
Vertretungsapp.

Vertretungsapp may, in its sole discretion, at any time and for any or no
reason, suspend or terminate this Agreement with or without prior notice.

This Agreement will terminate immediately, without prior notice from
Vertretungsapp, in the event that you fail to comply with any provision of
this Agreement. You may also terminate this Agreement by deleting the
Application and all copies thereof from your mobile device or from your
computer.

Upon termination of this Agreement, you shall cease all use of the Application
and delete all copies of the Application from your mobile device or from your
computer.

Termination of this Agreement will not limit any of Vertretungsapp's rights or
remedies at law or in equity in case of breach by you (during the term of this
Agreement) of any of your obligations under the present Agreement.

Amendments to this Agreement

Vertretungsapp reserves the right, at its sole discretion, to modify or
replace this Agreement at any time. If a revision is material we will provide
at least 30 days' notice prior to any new terms taking effect. What
constitutes a material change will be determined at our sole discretion.

By continuing to access or use our Application after any revisions become
effective, you agree to be bound by the revised terms. If you do not agree to
the new terms, you are no longer authorized to use the Application.

Governing Law

The laws of Lower Saxony, Germany, excluding its conflicts of law rules, shall
govern this Agreement and your use of the Application. Your use of the
Application may also be subject to other local, state, national, or
international laws.

Contact Information

If you have any questions about this Agreement, please contact us.